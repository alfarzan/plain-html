<?php 
include("dbconfig.php");
$status = "";
if (isset($_POST['btn_login'])) {
   $sql = "SELECT * from users";
  $result = mysqli_query($conn, $sql);
  while ($data = mysqli_fetch_assoc($result)) {
    if ($_POST['username'] == $data['name'] and $_POST['password']== $data['password']) {
    header("Location: admin.php");
    session_start();
    $_SESSION['id'] = $data['id'];
  }
  else{
    $status = "Invalid Credentials!";
  }
  }
  

  
}
?>

<!DOCTYPE html><!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Mon Nov 30 2020 19:11:01 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5f8d299bbc213a856fc7416c" data-wf-site="5f6c7aa4c99e322e7a2fda8d">
<head>
  <meta charset="utf-8">
  <title>Admin Login Page</title>
  <meta content="Apply today for the Global Data Science Competition &amp; work with real motor  insurance data organised by Imperial College London, Université du Québec à Montréal, Singapore Actuarial Society, Australian Actuarial Society, Institute and Faculty of Actuaries." name="description">
  <meta content="Admin Login Page" property="og:title">
  <meta content="Apply today for the Global Data Science Competition &amp; work with real motor  insurance data organised by Imperial College London, Université du Québec à Montréal, Singapore Actuarial Society, Australian Actuarial Society, Institute and Faculty of Actuaries." property="og:description">
  <meta content="Admin Login Page" property="twitter:title">
  <meta content="Apply today for the Global Data Science Competition &amp; work with real motor  insurance data organised by Imperial College London, Université du Québec à Montréal, Singapore Actuarial Society, Australian Actuarial Society, Institute and Faculty of Actuaries." property="twitter:description">
  <meta property="og:type" content="website">
  <meta content="summary_large_image" name="twitter:card">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/icl-insurance-pricing-game.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["PT Sans:400,400italic,700,700italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Red Hat Display:regular,italic,500,500italic,700,700italic,900,900italic","Mulish:200,300,regular,500,600,700,800,900,200italic,300italic,italic,500italic,600italic,700italic,800italic,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
</head>
<body class="body">
  <header id="Top" data-w-id="12f18177-cd81-7da7-9943-15de4eab5c71" style="opacity:0" class="main-container login">
    <div class="margin-div">
      <div class="section-2">
        <div data-collapse="medium" data-animation="default" data-duration="400" data-doc-height="1" role="banner" class="navbar w-nav">
          <a href="#" class="brand w-nav-brand"></a>
          <nav role="navigation" class="nav-menu w-nav-menu">
            <a href="#" class="nav-link w-nav-link">Active tracks</a>
            <a href="#" class="nav-link w-nav-link">Timelines</a>
            <a href="#" class="nav-link w-nav-link">Organizers</a>
            <div class="tip-text">Participate for opportunity to work with real Motor Insurance data</div>
          </nav>
          <div class="menu-button w-nav-button"></div>
        </div>
      </div>
    </div>
    <div class="margin-div bg"></div>
    <div class="margin-div login">
      <div data-w-id="837f47cd-72af-f436-75fa-b9733477d3c8" style="opacity:0" class="footer login">
        <h1 id="w-node-aff7775cfb2c-6fc7416c" class="heading-1 light login">Hello,<br><span class="text-span-3">enter your credentials to access the admin panel</span></h1>
        <!-- <div id="w-node-c38304cb536c-6fc7416c" class="w-form"> -->
          <form method="POST" id="email-form" name="email-form" data-name="Email Form" class="form-cf8 w-form">
            <div class="field-wrapper-cf8"><input type="text" class="text-field-cf8 w-input" maxlength="256" name="username" data-name="Username" placeholder="Username" id="Username" required=""></div>
            <div class="field-wrapper-cf8"></div>
            <div class="field-wrapper-cf8"><input type="password" class="text-field-cf8 w-input" maxlength="256" name="password" data-name="Password" placeholder="Password" id="Password" required=""></div><input type="submit" value="Login" name="btn_login" data-wait="Please wait..." class="button yellow w-button">
          </form>
          <h4 style="color: red"> <?= $status ?></h4>
          <div class="success-message-cf8 w-form-done">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="error-message-cf8 w-form-fail">
            <div>Oops! Something went wrong while submitting the form. <br>Please refresh and try again.</div>
          </div>
        <!-- </div> -->
      </div>
    </div>
    <a href="#Top" class="link-block w-inline-block"></a>
  </header>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5f6c7aa4c99e322e7a2fda8d" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>