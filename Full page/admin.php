<?php 
include("dbconfig.php");
session_start();
$sql = "SELECT count(*) from entries where status=0";
  $result = mysqli_query($conn, $sql);
  $data = mysqli_fetch_assoc($result);
  $unread = $data['count(*)'];

  $sql = "SELECT count(*) from entries where status=1";
  $result = mysqli_query($conn, $sql);
  $data = mysqli_fetch_assoc($result);
  $resolved = $data['count(*)'];

  if (!(isset($_SESSION['id']))) {
    header("Location: login.php");
  }
?>

<!DOCTYPE html><!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Mon Nov 30 2020 19:11:01 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5f8d0f4d5eb38620241ede87" data-wf-site="5f6c7aa4c99e322e7a2fda8d">
<head>
  <meta charset="utf-8">
  <title>Admin</title>
  <meta content="Admin" property="og:title">
  <meta content="Admin" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/icl-insurance-pricing-game.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["PT Sans:400,400italic,700,700italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Red Hat Display:regular,italic,500,500italic,700,700italic,900,900italic","Mulish:200,300,regular,500,600,700,800,900,200italic,300italic,italic,500italic,600italic,700italic,800italic,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
</head>
<body class="body">
  <header id="Top" data-w-id="12f18177-cd81-7da7-9943-15de4eab5c71" style="opacity:0" class="main-container">
    <div class="margin-div">
      <div class="section-2">
        <div data-collapse="medium" data-animation="default" data-duration="400" data-doc-height="1" role="banner" class="navbar w-nav">
          <a href="#" class="brand w-nav-brand"></a>
          <nav role="navigation" class="nav-menu w-nav-menu">
            <a href="#Unread" class="nav-link w-nav-link">Unread</a>
            <a href="#Deleted" class="nav-link w-nav-link">Deleted</a>
            <a href="#Resolved" class="nav-link w-nav-link">Resolved</a>
            <a href="#" onclick="logout()" class="nav-link w-nav-link">Logout</a>
            <div class="tip-text">Participate for opportunity to work with real Motor Insurance data</div>
          </nav>
          <div class="menu-button w-nav-button"></div>
        </div>
      </div>
      <div data-w-id="24abfcdf-d19a-1832-39df-5d3028488e04" style="opacity:0" class="section landing-screen admin">
        <div class="heading-6">Hello Ali!</div>
        <h1 class="heading-1">You have <span class="stat"><?= $unread ?></span> unread queries<br>&amp; <span class="stat"><?= $resolved ?></span> resolved queries</h1>
        <div class="divider"></div>
      </div>
      <section id="Unread" class="unread-queries">
        <div class="div-block-12">
          <div class="sub-heading admin">Unresponded queries</div>
        </div>
        <div class="table-row-div header">
          <div class="table-header">Sr No.</div>
          <div class="table-header">Email ID</div>
          <div class="table-header">Message</div>
          <div class="table-header">Action</div>
        </div>



<?php
  $sql = "SELECT * from entries where status=0";
  $result = mysqli_query($conn, $sql);
  $i=1;
  while ($data = mysqli_fetch_assoc($result)) {

   echo "<div class='table-row-div'>
            <div class='div-block-13'>
              <div class='table-header'>".$i."</div>
            </div>
            <div class='div-block-13'>
              <div class='table-header content email'>".$data['email']."</div>
              <div class='table-header date'>Received: ".date('d F Y', strtotime($data['createdOn']))."</div>
            </div>
            <div id='w-node-48299a50e36d-241ede87' class='table-header content'>".$data['content']."</div>
            <div id='w-node-48299a50e373-241ede87' class='div-block-11'>
              <a href='#' onclick='resolve(`".$data['id']."`)' class='action-buttons w-inline-block'></a>
              <a href='#' onclick='deleteentry(`".$data['id']."`)'  class='action-buttons delete w-inline-block'></a>
            </div>
          </div>";
          $i += 1;

   } 
?>

      </section>
      <section id="Resolved" class="unread-queries resolved">
        <div class="div-block-12">
          <div class="sub-heading admin">Resolved Queries</div>
        </div>
        <div class="table-row-div header resolved">
          <div class="table-header">Sr No.</div>
          <div class="table-header">Email ID</div>
          <div class="table-header">Message</div>
          <div class="table-header">Action</div>
        </div>




        <?php
  $sql = "SELECT * from entries where status=1";
  $result = mysqli_query($conn, $sql);
  $j = 1;
  while ($data = mysqli_fetch_assoc($result)) {

   echo "<div class='table-row-div responded'>
          <div class='div-block-13'>
            <div class='table-header'>".$j."</div>
          </div>
          <div class='div-block-13'>
            <div class='table-header content email'>".$data['email']."</div>
            <div class='table-header date'>Resolved On : ".date('d F Y', strtotime($data['updatedOn']))."</div>
            <div class='table-header date resolved'>Received: ".date('d F Y', strtotime($data['createdOn']))."</div>
          </div>
          <div id='w-node-a6c5e8812875-241ede87' class='table-header content'>".$data['content']."</div>
          <div id='w-node-a6c5e881287b-241ede87' class='div-block-11'>
            <a href='#' onclick='deleteentry(`".$data['id']."`)'  class='action-buttons delete w-inline-block'></a>
          </div>
        </div>";
        $j += 1;
   } 
?>
      </section>
      <section id="Deleted" class="unread-queries deleted">
        <div class="div-block-12">
          <div class="sub-heading admin">Deleted queries</div>
        </div>
        <div class="table-row-div header deleted">
          <div class="table-header">Sr No.</div>
          <div class="table-header">Email ID</div>
          <div id="w-node-1718a195a023-241ede87" class="table-header">Message</div>
        </div>


 <?php
  $sql = "SELECT * from entries where status=2";
  $result = mysqli_query($conn, $sql);
  $k = 1;
  while ($data = mysqli_fetch_assoc($result)) {

   echo " <div class='table-row-div deleted'>
          <div class='div-block-13'>
            <div class='table-header'>".$k."</div>
          </div>
          <div class='div-block-13'>
            <div class='table-header content email'>".$data['email']."</div>
            <div class='table-header date'>Deleted On : ".date('d F Y', strtotime($data['updatedOn']))."</div>
            <div class='table-header date deleted'>Received: ".date('d F Y', strtotime($data['createdOn']))."</div>
          </div>
          <div id='w-node-3e4f2ea0c147-241ede87' class='table-header content'>".$data['content']."</div>
          <div id='w-node-3e4f2ea0c14d-241ede87' class='div-block-11'>
            <a href='#' onclick='resolve(`".$data['id']."`)' class='action-buttons w-inline-block'></a>
          </div>
        </div>";
        $k +=1;

   } 
?>



    <!--   
        <div class="table-row-div deleted">
          <div class="div-block-13">
            <div class="table-header">001</div>
          </div>
          <div class="div-block-13">
            <div class="table-header content email">shreeneetrathi@gmail.com</div>
            <div class="table-header date">Received: 10 June 2020</div>
            <div class="table-header date deleted">Received: 10 June 2020</div>
          </div>
          <div id="w-node-a96dcec41b75-241ede87" class="table-header content"><strong><em>Lorem ipsum</em></strong>, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero&#x27;s De Finibus Bonorum et Malorum for use in a type specimen book.<br></div>
          <div id="w-node-a96dcec41b7b-241ede87" class="div-block-11">
            <a href="#" class="action-buttons w-inline-block"></a>
          </div>
        </div> -->
      </section>
    </div>
    <div class="margin-div bg"></div>
    <div class="margin-div"></div>
    <a href="#Top" class="link-block w-inline-block"></a>
  </header>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5f6c7aa4c99e322e7a2fda8d" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

  <script type="text/javascript">
    function resolve(argument) {
       $.ajax({
          type: 'GET',
          url: './api/resolve.php',
          data: '&id='+argument,
          success: function(data) {
            if (data == "1") {
             location.reload();
            }
          }
        });
    }

    function deleteentry(argument) {
      // alert(argument)
       $.ajax({
          type: 'GET',
          url: './api/delete.php',
          data: '&id='+argument,
          success: function(data) {
            // alert(data)
            if (data == "1") {
              location.reload();
            }
          }
        });
    }

    function logout(){
      $.ajax({
          type: 'GET',
          url: './api/logout.php',
          success: function(data) {
            // alert(data)
            if (data == "1") {
              location.reload();
            }
          }
        });
    }
  </script>
</body>
</html>